from django.urls import path
from tasks.views import TaskCreateView, TaskListView, task_update


urlpatterns = [
    path("create/", TaskCreateView.as_view(), name="create_task"),
    path("mine/", TaskListView.as_view(), name="show_my_tasks"),
    path("<int:pk>/complete/", task_update, name="complete_task"),
]
