from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from tasks.models import Task
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.
class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    template_name = "tasks/create.html"

    def get_success_url(self) -> str:
        return reverse_lazy("show_project", args=[self.object.project.id])


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


def task_update(request, pk):
    complete = request.POST.get("is_completed")
    task = Task.objects.get(pk=pk)
    task.is_completed = complete
    task.save()
    return redirect("show_my_tasks")
